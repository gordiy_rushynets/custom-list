// CustomList.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "List.h"

int main()
{
	List<int> a;

	a.insert(44, 0);
	a.push_front(1);
	a.push_back(4);
	a.push_back(7);

	for (int i = 0; i < a.get_size(); i++)
	{
		std::cout << a[i] << endl;
	}
	system("pause");
	return 0;
}
