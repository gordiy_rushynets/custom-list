#pragma once

using namespace std;

template<typename T>
class List
{
private:
	template<typename T>
	class Node
	{
	public:
		Node* pNext;
		T data;

		Node(T data = T(), Node *pNext = nullptr)
		{
			this->data = data;
			this->pNext = pNext;
		}
	};

	int Size;
	Node<T> *head;
public:
	List();
	~List();

	void push_back(T data);
	void push_front(T data);
	void insert(T data, int index);

	void pop_front();
	void clear();
	int get_size();

	T& operator[](const int index); 

};


template<typename T>
List<T>::List()
{
	Size = 0;
	head = nullptr;
}

template<typename T>
List<T>::~List()
{
	clear();
}

// must add elements into list
template<typename T>
inline void List<T>::push_back(T data)
{
	// if address first element into list is nullptr we add element as head
	if (head == nullptr)
	{
		head = new Node<T>(data);
	}
	else
	{
		// create current which save address first elem in list
		Node<T> *current = this->head;
		// check all addresses into list
		while (current->pNext != nullptr)
		{
			// exchange address for current element
			current = current->pNext;
		}
		// save new data in the end of list
		current->pNext = new Node<T>(data);
	}
	Size++;
}

template<typename T>
inline void List<T>::push_front(T data)
{
	// create new head, set new data as first element in list and pNext is previous head
	head = new Node<T>(data, head);
	Size++;
}

template<typename T>
inline void List<T>::insert(T data, int index)
{
	if (index == 0)
	{
		push_front(data);
	}
	else
	{
		Node<T> *previous = this->head;

		for (int i = 0; i < index - 1; i++)
		{
			previous = previous->pNext;
		}

		Node<T> *newNode = new Node<T>(data, previous->pNext);
		previous->pNext = newNode;

		Size++;
	}

}

// must remove first element (head) in list
template<typename T>
inline void List<T>::pop_front()
{
	Node<T> *exchange_elem = head;

	head = head->pNext;
	delete exchange_elem;
	Size--;
}


// must remove all elements from our list
template<typename T>
inline void List<T>::clear()
{
	while (Size)
	{
		pop_front();
	}
}

template<typename T>
inline int List<T>::get_size()
{
	return Size;
}

// overload operator for indexes
template<typename T>
inline T & List<T>::operator[](const int index)
{
	int counter = 0;
	Node<T> *current = this->head;
	while (current != nullptr)
	{
		if (counter == index)
		{
			return current->data;
		}
		current = current->pNext;
		counter++;
	}
}

